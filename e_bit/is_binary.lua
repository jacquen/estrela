Bit.isBinary = function (str)
    for i = 1, #str do
        local char = str:sub(i, i)
        if (string.byte(char) < 32) then
            return true
        end
    end
    return false
end
