local hexStringToBinaryStringTable = {
	["0"] = "0000",
	["1"] = "0001",
	["2"] = "0010",
	["3"] = "0011",
	["4"] = "0100",
	["5"] = "0101",
	["6"] = "0110",
	["7"] = "0111",
	["8"] = "1000",
	["9"] = "1001",
	["a"] = "1010",
	["b"] = "1011",
	["c"] = "1100",
	["d"] = "1101",
	["e"] = "1110",
	["f"] = "1111"
}

local function handleBinaryStringLength(binary, length)
    local difference = length - #binary
    if (difference < 1) then
        return binary:sub(#binary - length + 1, #binary)
    else
        return String.duplicate("0", difference) .. binary
    end
end

local function hexStringToBinaryString(hexString, length)
    local binary = ""
    for i = 1, #hexString do
        local char = hexString:sub(i, i)
        binary = binary .. hexStringToBinaryStringTable[string.lower(char)]
    end
    if (length) then
        return handleBinaryStringLength(binary, length)
    else
        return binary
    end
end

local function numberToBinaryString(number, length)
    local hexString = string.format("%x", number)
    return hexStringToBinaryString(hexString, length)
end

-- Can take as input a hexadecimal string or a number.
Bit.binaryString = function (input, length)
    if (type(input) == "string") then
        return hexStringToBinaryString(input, length)
    else
        return numberToBinaryString(input, length)
    end
end

local function getBinaryStringsOfEqualLength(a, b)
    local binaryA = Bit.binaryString(a)
    local binaryB = Bit.binaryString(b)
    if (#binaryA > #binaryB) then
        binaryB = Bit.binaryString(b, #binaryA)
    else
        binaryA = Bit.binaryString(a, #binaryB)
    end
    return binaryA, binaryB
end

local function bitAnd(a, b)
    return (a == "1" and b == "1") and "1" or "0"
end

Bit.bAnd = function (a, b)
    local binaryA, binaryB = getBinaryStringsOfEqualLength(a, b)
    local andString = ""
    for i = 1, #binaryA do
        andString = andString .. bitAnd(binaryA:sub(i, i), binaryB:sub(i, i))
    end
    return tonumber(andString, 2)
end

local function bitOr(a, b)
    return (a == "1" or b == "1") and "1" or "0"
end

Bit.bOr = function (a, b)
    local binaryA, binaryB = getBinaryStringsOfEqualLength(a, b)
    local orString = ""
    for i = 1, #binaryA do
        orString = orString .. bitOr(binaryA:sub(i, i), binaryB:sub(i, i))
    end
    return tonumber(orString, 2)
end

-- word[number]: binary word (0-indexed) to be inspected; bitIndex[number]: bit 
-- index in the word to be inspected; returns boolean for whether or not the bit 
-- is "on" in the word. E.g. (10 = 0b1010) Bit.bOn(10, 1) = true; 
-- Bit.bOn(10, 2) = false
Bit.bOn = function (word, bitIndex)
    local tester = 2 ^ bitIndex
    local andValue = Bit.bAnd(word, tester)
    return andValue == tester
end
