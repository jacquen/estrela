Bit.hexDump = function (packedString)
    if (not(packedString) or #packedString < 1) then return "" end
    local formats = ""
    for i = 1, #packedString do
        formats = formats .. "b"
    end
    local bytes = Bit.unpack(packedString, formats, {tableValues = true})
    local hexDumpString = ""
    for _, byte in pairs(bytes) do
        hexDumpString = hexDumpString .. " " .. string.format("%02X", byte)
    end
    return string.sub(hexDumpString, 2, #hexDumpString)
end
