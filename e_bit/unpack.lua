local function binaryToDecimal(binary, bytes)
    local hexString = ""
    for i = 1, bytes do
        local binaryByte = binary:sub(i, i)
        local decimalByte = string.byte(binaryByte)
        local hexByte = string.format("%02x", decimalByte)
        hexString = hexByte .. hexString
    end
    return tonumber(hexString, 16)
end

local function unpackStringValue(binary, index, stringLength)
    local stringValue = ""
    for i = 1, stringLength do
        local binaryByte = binary:sub(index + i, index + i)
        local decimalByte = string.byte(binaryByte)
        local stringByte = string.char(decimalByte)
        stringValue = stringValue .. stringByte
    end
    return stringValue, stringLength + 1
end

local function unpackString(binary, index)
    local stringLength = string.byte(binary:sub(index, index))
    if (index > #binary) then
        return nil, stringLength + 1
    else
        return unpackStringValue(binary, index, stringLength)
    end
end

local function unpackNumber(binary, format, index)
    local byteLength = Bit.formatByteLength[format]
    if (index > #binary) then
        return nil, byteLength
    else
        local binaryValue = binary:sub(index, index - 1 + byteLength)
        local decimalValue = binaryToDecimal(binaryValue, byteLength)
        return decimalValue, byteLength
    end
end

local function unpackValue(binary, format, index)
    if (format == "p") then
        return unpackString(binary, index)
    else
        return unpackNumber(binary, format, index)
    end
end

local function formatValues(values, options)
    if (options.tableValues) then
        return values
    else
        return unpack(values)
    end
end

local function handleReturnOptions(index, values, options)
    if (options.returnIndex) then 
        return index, formatValues(values, options)
    else
        return formatValues(values, options)
    end
end

Bit.unpack = function (binary, formats, options)
    options = options or {}
    local index = options.index or 1
    local values = {}
    for i = 1, #formats do
        local format = formats:sub(i, i)
        local value, byteLength = unpackValue(binary, format, index)
        values[i] = value
        index = index + byteLength
    end
    return handleReturnOptions(index, values, options)
end
