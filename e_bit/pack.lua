local function decimalToHexString(decimal, bytes)
    local hexString = string.format("%08x", decimal)
    local stringLength = 2 * bytes
    return hexString:sub(9 - stringLength, 8)
end

local function decimalToBinary(decimal, bytes)
    local hexString = decimalToHexString(decimal, bytes)
    local binary = ""
    for i = 1, #hexString, 2 do
        local hexByte = hexString:sub(i, i + 1)
        local decimalByte = tonumber(hexByte, 16)
        binary = string.char(decimalByte) .. binary
    end
    return binary
end

local function packString(str)
    return string.char(#str) .. str
end

local function packValue(value, format)
    if (format == "p") then
        return packString(value)
    else
        return decimalToBinary(value, Bit.formatByteLength[format])
    end
end

Bit.pack = function (formats, ...)
    local args = {...}
    local binary = ""
    for i = 1, #formats do
        local format = formats:sub(i, i)
        local argBinary = packValue(args[i], format)
        binary = binary .. argBinary
    end
    return binary
end

Bit.packHexString = function (hexStringLittleEndian)
    hexStringLittleEndian = hexStringLittleEndian:gsub("%s+", "")
    local hexString = string.reverse(hexStringLittleEndian)
    local binary = ""
    for i = 1, #hexString, 2 do
        local hexByteLittleEndian = hexString:sub(i, i + 1)
        local hexByte = string.reverse(hexByteLittleEndian)
        local decimalByte = tonumber(hexByte, 16)
        binary = string.char(decimalByte) .. binary
    end
    return binary
end
