Bit = {}

Bit.formatByteLength = {
    b = 1,
    h = 2,
    H = 2,
    f = 4,
    I = 4,
    l = 4,
    L = 4,
    d = 8
}
