-- Notes:
-- The console currently cannot handle multi-line blocks.
-- If a directory contains no files, the console will print "File Not Found"
-- when it loads it (for Windows).

local short_src = debug.getinfo(1).short_src
local directory = short_src:sub(3, -12)
ESTRELA_CALL_PATH = directory:gsub("\\", "."):gsub("/", ".")

function estrelaConsole(ignoreDirectories, ignoreFiles)
    require(ESTRELA_CALL_PATH .. "e_module.all")

    Module.initialize(ignoreDirectories, ignoreFiles)

    local shouldExit = false

    local function handleSpecialInput(input)
        if (input == "exit") then
            shouldExit = true
            return true
        elseif (input == "cls") then
            os.execute("cls")
            return true
        elseif (input == "reload") then
            Module.reload()
            return true
        end
    end

    local function textAfterColon(text)
        local _, index = text:find("%:")
        return text:sub(index + 1, #text)
    end

    local function handleError(err)
        local first = textAfterColon(err)
        local second = textAfterColon(first)
        print("Error:" .. second)
    end

    local function handleUnprintableCode(input)
        local status, err = pcall(function ()
            loadstring(input)()
        end)
        if (err) then
            handleError(err)
        end
    end

    local function handleNoInput(input)
        if not (input) then
            shouldExit = true
            return true
        end
    end

    local function handleCodeInput(input)
        local status, err = pcall(function ()
            loadstring("print(" .. input .. ")")()
        end)
        if (err) then
            handleUnprintableCode(input)
        end
    end

    local function runConsole()
        io.write("> ")
        local input = io.read()
        if (handleNoInput(input)) then return end
        if (handleSpecialInput(input)) then return end
        handleCodeInput(input)
    end

    local function startConsole()
        print("-------------------")
        print("Lua Console")
        print("-------------------")
        while not (shouldExit) do
            -- local status, err = pcall(runConsole)
            runConsole()
        end
    end

    startConsole()
end
