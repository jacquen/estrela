Test.failures = 0
Test.lastTestFailed = ""

function expect(expectation, ...)
    if ((...) ~= nil) then
        error("expect() takes only one argument but received more than one.")
    end
    if not (expectation) then
        if (Test.lastTestFailed == Test.currentTest) then return end
        Test.lastTestFailed = Test.currentTest
        Test.failures = Test.failures + 1
        local info = debug.getinfo(2)
        print("")
        print("*******************")
        print("Expectation Failed.")
        print("- - - - - - - - - -")
        print("       Test:  " .. Test.currentTest)
        print("Description:  " .. Test.currentTestDescription)
        print("       Path:  " .. info.source)
        print("Line number:  " .. info.currentline)
        print("*******************")
    end
end
