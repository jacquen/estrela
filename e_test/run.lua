Test.run = function (arg)        
    local options = {}
    for key, value in pairs(arg) do
        options[value] = true
    end

    if (options["-log"] or options["-l"]) then
        Test.shouldLog = true
    end

    Test.runTestSuite(options)
end
