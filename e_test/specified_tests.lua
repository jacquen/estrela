Test.specifiedTests = {}

local function fillParentTest(option)
    if (Tests[option]) then
        table.insert(Test.specifiedTests, {parentName = option, all = true})
    end
end

local function childTestInfo(parentName, childName)
    return {parentName = parentName, all = false, childName = childName}
end

local function fillChildTest(option)
    local _, parentEnd = option:find("%.")
    if (parentEnd) then
        local parentName = option:sub(1, parentEnd - 1)
        local childName = option:sub(parentEnd + 1, #option)
        if (Tests[parentName]) then
            local info = childTestInfo(parentName, childName)
            table.insert(Test.specifiedTests, info)
        end
    end
end

Test.fillSpecifiedTests = function (options)
    for option, _ in pairs(options) do
        fillParentTest(option)
        fillChildTest(option)
    end
end
