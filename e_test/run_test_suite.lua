Test.runTestSuite = function (options)
    Test.fillSpecifiedTests(options)
    Test.runTests()
    Test.printTestSuiteDone()
end
