local function printFailedSuite()
    print("XXXXXXXXXXXXXXXXXXXXXXXX")
    print("XX Test Suite Failed. XX")
    print("XXXXXXXXXXXXXXXXXXXXXXXX")
end

local function printSuccessfulSuite()
    print("++++++++++++++++++++++++")
    print("++ Test Suite Passed. ++")
    print("++++++++++++++++++++++++")
end

local function printStats()
    print(
        "Passed " .. (Test.testsRun - Test.failures) .. " out of " 
        .. Test.testsRun .. " tests."
    )
end

Test.printTestSuiteDone = function ()
    print("")
    if (Test.failures > 0) then
        printFailedSuite()
    else
        printSuccessfulSuite()
    end
    printStats()
end
