Test.testsRun = 0

local function logRunningTest()
    if (Test.shouldLog) then 
        print("=============")
        print("Running Test: " .. Test.currentTest) 
        print("=============")
    else
        io.write(".")
    end
end

local function runTest(parentName, name, test)
    Test.currentTest = parentName .. "." .. name
    logRunningTest()
    describe("")
    test()
    Test.testsRun = Test.testsRun + 1
end

local function runAllTestsInParent(parentName, parentTest)
    for name, test in pairs(parentTest()) do
        runTest(parentName, name, test)
    end
end

local function runSpecifiedTest(info)
    if (info.all) then
        runAllTestsInParent(info.parentName, Tests[info.parentName])
    else
        local childTests = Tests[info.parentName]()
        local test = childTests[info.childName]
        if not (test) then return end
        runTest(info.parentName, info.childName, test)
    end
end

Test.runTests = function ()
    if (Table.size(Test.specifiedTests) > 0) then
        for _, info in pairs(Test.specifiedTests) do
            runSpecifiedTest(info)
        end
    else
        for parentName, parentTest in pairs(Tests) do
            runAllTestsInParent(parentName, parentTest)
        end
    end
end
