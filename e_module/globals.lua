local original_G = {
    original_G = true
}

local function makeOriginal_G()
    for key, _ in pairs(_G) do
        original_G[key] = true
    end
end

Module.initializeGlobals = function ()
    makeOriginal_G()
end

local estrelaGlobals = {
    "Bit", "Data", "File", "Module", "String", "System", "Table"
}

Module.printGlobals = function ()
    local truthifiedEstrelaGlobals = Table.truthify(estrelaGlobals)
    for name, _ in pairs(_G) do
        if not (original_G[name] or truthifiedEstrelaGlobals[name]) then
            print(name)
        end
    end
end

local function clearUnoriginalGlobals()
    for name, _ in pairs(_G) do
        if not (original_G[name]) then
            _G[name] = nil
        end
    end
end

Module.resetGlobals = function ()
    clearUnoriginalGlobals()
end
