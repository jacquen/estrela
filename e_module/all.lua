Module = {}

if not (ESTRELA_CALL_PATH) then
    local short_src = debug.getinfo(1).short_src
    local directory = short_src:sub(3, -17)
    ESTRELA_CALL_PATH = directory:gsub("\\", "."):gsub("/", ".")
end

require(ESTRELA_CALL_PATH .. "e_module.globals")
require(ESTRELA_CALL_PATH .. "e_module.initialize")
require(ESTRELA_CALL_PATH .. "e_module.prerequire")
