-- Note: If Module.initialize is called through a chain of many levels, some of those
-- files may need to be passed in as ignoreFiles.

local IGNORE_DIRECTORIES, IGNORE_FILES

Module.required = {init = {}, regular = {}}

local function requireFile(name)
    if (Module.blackList[name] or IGNORE_FILES[name]) then return end
    if (name:sub(#name - 5, #name) == "._init") then
        Module.required.init[name] = true
    else
        Module.required.regular[name] = true
    end
    require(name)
end

local function requireLuaFile(file)
    local length = #file
    if not (file:sub(length - 3, length) == ".lua") then return end
    local fileName = file:sub(1, length - 4)
    requireFile(fileName)
end

local function requireLuaFiles(files)
    for _, file in pairs(files) do
        requireLuaFile(file)
    end
end

local function handleInitFile(files, filePrefix)
    local filesWithoutInits = {}
    for _, fileName in pairs(files) do
        if (fileName == "_init.lua") then
            requireFile(filePrefix .. "_init")
        else
            table.insert(filesWithoutInits, fileName)
        end
    end
    return filesWithoutInits
end

local function getStartPaths()
    local startPaths = {}
    for i = 3, 4 do
        local info = debug.getinfo(i)
        local short_src = info.short_src
        local directory = short_src:sub(3, -5)
        local path = directory:gsub("\\", "."):gsub("/", ".")
        table.insert(startPaths, path)
    end
    return startPaths
end

local function requireAllFiles(contents, filePrefix)
    filePrefix = filePrefix or ""
    local filesWithoutInits = handleInitFile(contents.files, filePrefix)
    local prefixedFiles = File.prefixFiles(filesWithoutInits, filePrefix)
    requireLuaFiles(prefixedFiles)
    for key, value in pairs(contents) do
        if not (key == "files" or IGNORE_DIRECTORIES[filePrefix .. key]) then
            requireAllFiles(value, filePrefix .. key .. ".")
        end
    end
end

Module.initialize = function (ignoreDirectories, ignoreFiles)
    local startPaths = getStartPaths()
    Module.prerequire(startPaths)
    IGNORE_DIRECTORIES = Table.truthify(ignoreDirectories)
    IGNORE_FILES = Table.truthify(ignoreFiles)
    Module.initializeGlobals()
    local contents = File.directoryContentsRecursive()
    requireAllFiles(contents)
end

local function unloadModules(modules)
    for name, _ in pairs(modules) do
        package.loaded[name] = nil
    end
end

Module.reload = function ()
    Module.resetGlobals()
    unloadModules(Module.required.init)
    unloadModules(Module.required.regular)
    Module.required = {init = {}, regular = {}}
    local contents = File.directoryContentsRecursive()
    requireAllFiles(contents)
end
