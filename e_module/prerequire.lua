local prerequiredFileShorts = {
    "e_table._init", "e_table.flip", "e_table.concat",
    "e_table.truthify", "e_file._init", "e_file.format_windows_path",
    "e_file.directory_contents"
}

local function buildPrerequiredFiles()
    local prerequiredFiles = {}
    for _, file in pairs(prerequiredFileShorts) do
        table.insert(prerequiredFiles, ESTRELA_CALL_PATH .. file)
    end
    return prerequiredFiles
end

local blackListShorts = {
    "console", "e_module.all", "e_module.globals", "e_module.initialize",
    "e_module.prerequire"
}

Module.blackList = {}

local function fillBlackList()
    for _, file in pairs(blackListShorts) do
        Module.blackList[ESTRELA_CALL_PATH .. file] = true
    end
end

Module.prerequire = function (startPaths)
    for _, startPath in pairs(startPaths) do
        Module.blackList[startPath] = true
    end
    local prerequiredFiles = buildPrerequiredFiles()
    fillBlackList()
    for _, file in ipairs(prerequiredFiles) do
        require(file)
        Module.blackList[file] = true
    end
end
