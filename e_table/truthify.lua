Table.truthify = function (tbl)
    tbl = tbl or {}
    local options = {}
    for key, option in pairs(tbl) do
        options[option] = true 
    end
    return options
end
