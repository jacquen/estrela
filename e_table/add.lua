Table.add = function (tbl, key, value)
    local cloneTbl = Data.clone(tbl)
    cloneTbl[key] = value
    return cloneTbl
end
