local tableString

local function addToTableString(line)
    tableString = tableString .. "\n" .. line
end

local library = {}

library.isValidTable = function (tbl)
    if (type(tbl) == "table") then return true end
    print("Table.print(): Non-table value given.")
end

local function formatKey(key)
    if (type(key) ~= "string") then 
        return "[" .. tostring(key) .. "]"
    end
    return '["' .. tostring(key) .. '"]'
end

local function makeFormatting(indent, key)
    return string.rep("    ", indent) .. formatKey(key) .. " = "
end

local function handleTable(formatting, value, indent, isLastIndex)
    addToTableString(formatting .. "{")
    library.handleTableContents(value, indent + 1)
    local finalMarks = isLastIndex and "}" or "},"
    addToTableString(string.rep("    ", indent) .. finalMarks)
end

local function handleBinary(value)
    local maxLength = 10
    local tooLong = #value > maxLength
    local shortenedValue = value:sub(1, maxLength)
    local suffix = tooLong and "..." or ""
    return "BINARY: " .. Bit.hexDump(shortenedValue) .. suffix
end

local function handleStringTypes(value)
    if (Bit.isBinary(value)) then
        return handleBinary(value)
    end
    local maxLength = 80
    if (#value > maxLength) then
        return value:sub(1, maxLength) .. "..."
    end
    return value
end

local function handleNonTableTypes(value)
    if (type(value) == "string") then
        return '"' .. handleStringTypes(value) .. '"'
    end
    return value
end

local function handleNonTable(formatting, value, isLastIndex)
    local valueToPrint = handleNonTableTypes(value)
    local finalMarks = isLastIndex and "" or ","
    addToTableString(formatting .. tostring(valueToPrint) .. finalMarks)
end

local isShallow

local function handleValue(value, formatting, indent, index, length)
    if (type(value) == "table" and not(isShallow)) then
        handleTable(formatting, value, indent, index == length)
    else
        handleNonTable(formatting, value, index == length)
    end
end

library.handleTableContents = function (tbl, indent)
    local length = Table.size(tbl)
    local index = 0
    for key, value in pairs(tbl) do
        index = index + 1
        handleValue(value, makeFormatting(indent, key), indent, index, length)
    end
end

Table.toString = function (tbl, options)
    tableString = ""
    options = Table.truthify(options)
    if not (library.isValidTable(tbl)) then return end
    local tableToHandle = options.sort and Table.sortByKeys(tbl) or tbl
    isShallow = options.shallow
    handleTable("", tableToHandle, 0, true)
    return tableString
end

Table.print = function (tbl, options)
    print(Table.toString(tbl, options))
end
