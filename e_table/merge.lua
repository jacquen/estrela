Table.merge = function (secondaryTable, primaryTable)
    local mergedTable = {}
    for key, value in pairs(secondaryTable) do
        mergedTable[key] = Data.clone(value)
    end
    for key, value in pairs(primaryTable) do
        mergedTable[key] = Data.clone(value)
    end
    return mergedTable
end
