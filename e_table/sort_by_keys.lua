Table.sortByKeys = function (tbl)
    local keysArray = {}
    for key, _value in pairs(tbl) do
        table.insert(keysArray, key)
    end
    local sortedKeys = table.sort(Data.clone(keysArray))
    local sortedTable = {}
    for _, key in pairs(keysArray) do
        sortedTable[key] = tbl[key]
    end
    return sortedTable
end
