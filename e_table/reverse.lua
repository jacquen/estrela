Table.reverse = function (tbl)
    local reversedTable = {}
    for _key, value in pairs(tbl) do
        table.insert(reversedTable, 1, value)
    end
    return reversedTable
end
