Table.flip = function (tbl)
    local flippedTable = {}
    for key, value in pairs(tbl) do
        flippedTable[value] = key
    end
    return flippedTable
end
