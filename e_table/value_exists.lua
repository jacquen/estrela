Table.valueExists = function (tbl, value)
    local tableWithValuesAsKeys = Table.flip(tbl)
    return tableWithValuesAsKeys[value] ~= nil
end
