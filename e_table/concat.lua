-- This function is to be used for arrays only.
Table.concat = function (t1, t2)
    local concatenatedTable = {unpack(t1)}
    for _, value in ipairs(t2) do
        table.insert(concatenatedTable, value)
    end
    return concatenatedTable
end
