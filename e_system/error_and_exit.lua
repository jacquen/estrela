local function previousTraceback(traceback)
    local firstTrace = String.capture(traceback, "traceback:\n", "\n")
    local startIndex, endIndex = String.findEscaped(traceback, firstTrace)
    return (
        traceback:sub(1, startIndex - 1) .. 
        traceback:sub(endIndex + 2, #traceback)
    )
end

System.errorAndExit = function (message)
    print("ERROR: " .. message)
    print(previousTraceback(debug.traceback()))
    if (os.exit) then
        os.exit()
    end
end
