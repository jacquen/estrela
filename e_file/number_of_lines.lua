File.numberOfLines = function (filePath)
    local lines = 0
    for _ in io.lines(filePath) do
        lines = lines + 1
    end
    return lines
end
