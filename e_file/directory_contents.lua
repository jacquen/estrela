local os = package.config:sub(1,1) == "/" and "unix" or "windows"

local function formatName(name)
    local length = #name
    if (name:sub(length, length) == "/") then
        return name:sub(1, length - 1)
    end
    return name
end

local function buildContents(handle)
    local contents = {}
    for name in handle:lines() do
        if not (name:sub(1, 1) == ".") then
            table.insert(contents, formatName(name))
        end
    end
    return contents
end

local function getUnixContentsCommand(path, options)
    if (options.files) then
        return "ls -p " .. path .. " | grep -v /"
    elseif (options.directories) then
        return "ls -F " .. path .. " | grep /"
    end
end

local function contentsInDirectoryUnix(path, options)
    local formattedPath = path and path or ""
    local command = getUnixContentsCommand(formattedPath, options)
    local handle = io.popen(command)
    local contents = buildContents(handle)
    handle:close()
    return contents
end

local function getDirOptions(options)
    if (options.files) then
        return " /a-d"
    elseif (options.directories) then
        return " /ad"
    end
end

local function contentsInDirectoryWindows(path, options)
    local dirOptions = getDirOptions(options)
    local formattedPath = File.formatWindowsPath(path and path or "")
    local handle = io.popen('dir "'.. formattedPath ..'" /b' .. dirOptions)
    local contents = buildContents(handle)
    handle:close()
    return contents
end

local function contentsInDirectory(path, options)
    options = Table.truthify(options)
    if (os == "unix") then
        return contentsInDirectoryUnix(path, options)
    else
        return contentsInDirectoryWindows(path, options)
    end
end

File.directoriesInDirectory = function (path)
    return contentsInDirectory(path, {"directories"})
end

File.prefixFiles = function (files, prefix)
    prefix = prefix or ""
    local prefixedFiles = {}
    for _, file in pairs(files) do
        table.insert(prefixedFiles, prefix .. file)
    end
    return prefixedFiles
end

local function filesInDirectoryRecursive(path, allFiles, filePrefix)
    allFiles = allFiles or {}
    local contents = File.directoryContents(path)
    allFiles = Table.concat(
        allFiles, File.prefixFiles(contents.files, filePrefix)
    )
    for _, directory in pairs(contents.directories) do
        allFiles = filesInDirectoryRecursive(
            directory, allFiles, directory .. "."
        )
    end
    return allFiles
end

File.filesInDirectoryRecursive = function (path)
    return filesInDirectoryRecursive(path)
end

File.filesInDirectory = function (path)
    return contentsInDirectory(path, {"files"})
end

File.directoryContents = function (path)
    return {
        directories = File.directoriesInDirectory(path),
        files = File.filesInDirectory(path)
    }
end

File.directoryContentsRecursive = function (path)
    local allContents = {}
    local contents = File.directoryContents(path)
    allContents.files = contents.files
    for _, directory in pairs(contents.directories) do
        local prefix = path and path .. "/" or ""
        allContents[directory] = File.directoryContentsRecursive(
            prefix .. directory
        )
    end
    return allContents
end
