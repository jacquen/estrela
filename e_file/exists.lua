File.exists = function (path)
    local file = io.open(path)
    if (file) then
        file:close()
        return true
    end
    return false
end
