File.formatWindowsPath = function (path)
    if (path:sub(1, 2) == "./") then
        path = path:sub(3, #path)
    end
    return path:gsub("/", "\\\\")
end
