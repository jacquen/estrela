local os = package.config:sub(1,1) == "/" and "unix" or "windows"

local function getCommand(directoryPath)
    if (os == "unix") then
        return "ls " .. directoryPath .. " -F |grep -v / | wc -l"
    else
        local formattedPath = File.formatWindowsPath(directoryPath)
        return 'dir ' .. formattedPath .. ' /b /a-d | find /c /v ""'
    end
end

-- njdebug: need to test this in Yale driver on Controller
File.numberOfFilesInDirectory = function (directoryPath)
    command = getCommand(directoryPath)
    handle = io.popen(command)
    result = handle:read("*a")
    handle:close()
    return tonumber(result)
end
