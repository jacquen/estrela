local function cloneTable(tbl)
    local clone = {}
    for key, value in pairs(tbl) do
        if type(value) == "table" then
            value = cloneTable(value)
        end
        clone[key] = value
    end
    return clone
end

Data.clone = function (value)
    if (type(value) == "table") then
        return cloneTable(value)
    end
    return value
end
