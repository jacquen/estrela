-- values parameter is an array.
Data.isEquivalentToAny = function (mainValue, values)
    if (mainValue == nil) then
        error("isEquivalentToAny(): mainValue cannot be nil.")
    end
    for _key, value in pairs(values) do
        if (mainValue == value) then
            return true
        end
    end
    return false
end
