String.duplicate = function (str, times)
    local duplicatedString = ""
    for i = 1, times do
        duplicatedString = duplicatedString .. str
    end
    return duplicatedString
end
