-- xml and tagName parameters are a strings.

String.xmlCapture = function (xml, tagName)
    local openTag = "<" .. tagName .. ">"
    local closeTag = "</" .. tagName .. ">"
    return String.capture(xml, openTag, closeTag)
end

String.xmlCaptureAll = function (xml, tagName)
    local openTag = "<" .. tagName .. ">"
    local closeTag = "</" .. tagName .. ">"
    return String.captureAll(xml, openTag, closeTag)
end
