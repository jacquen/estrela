local escapePattern = '(['..("%^$().[]*+-?"):gsub("(.)", "%%%1")..'])'

local function escapeSpecialChars(str)
    local escapedString = str:gsub(escapePattern, "%%%1")
    return escapedString
end

String.findEscaped = function (containerString, queryString)
    return containerString:find(escapeSpecialChars(queryString))
end
