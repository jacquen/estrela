local function getStrWithoutLeftBound(str, leftBound)
    if not (str) then return end
    local _, startIndex = String.findEscaped(str, leftBound)
    if not (startIndex) then return end
    startIndex = startIndex + 1
    return str:sub(startIndex, #str)
end

local function getCapture(str, rightBound)
    local endIndex, _ = String.findEscaped(str, rightBound)
    if not (endIndex) then return end
    endIndex = endIndex - 1
    return str:sub(1, endIndex)
end

local function getRemainingString(str, captureWithBounds)
    local _, startIndex = String.findEscaped(str, captureWithBounds)
    if not (startIndex) then return "" end
    startIndex = startIndex + 1
    return str:sub(startIndex, #str)
end

String.capture = function (str, leftBound, rightBound)
    local strNoLeftBound = getStrWithoutLeftBound(str, leftBound)
    if not (strNoLeftBound) then return nil, "" end
    local capture = getCapture(strNoLeftBound, rightBound)
    if not (capture) then return nil, "" end
    local captureWithBounds = leftBound .. capture .. rightBound
    local remainingString = getRemainingString(str, captureWithBounds)
    return capture, remainingString
end

String.captureAll = function (str, leftBound, rightBound, captures)
    captures = captures or {}
    local capture, remainingString = String.capture(str, leftBound, rightBound)
    if not (capture) then return captures end
    captures[#captures + 1] = capture
    if (#remainingString > 0) then
        return String.captureAll(
            remainingString, leftBound, rightBound, captures
        )
    end
    return captures
end
