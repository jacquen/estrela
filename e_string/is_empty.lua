String.isEmpty = function (str)
    return not (str ~= nil and str:match("%S") ~= nil)
end
